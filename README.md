# MyNXT documentation

This repository contains the documentation for all MyNXT services.

## Guide

We use [MkDocs](http://www.mkdocs.org/) to create the documentation.