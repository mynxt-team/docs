#Welcome to the myNXT API

The myNXT.info API allows you to interact with the myNXT.info online wallet, as well as perform standard NXT queries. We run a network of at least 10 high-performance NRS servers, spread around the world on four continents and monitored 24/7. When you make a call to the myNXT.info API, we automatically determine the best server to connect you to in order to process your request.

---

## Authentication

You will need a valid user id in order to use the myNXT API. You can create one for free at https://wallet.mynxt.info. The user authentication for the API varies depending on whether you have 2FA enabled.

If 2FA is disabled: simply call the API endpoint with the desired user email and password
If 2FA is enabled:
Make a call to any of the API endpoints with the user email and password as normal
The API will respond with a message indicating that this wallet user has 2FA enabled
Create a unique id to represent this user in your system, for example 'USER123'.You will need to send this id to the API in every call
Call the Auth API endpoint (see below) with the user email + user password + unique user ID + Google Auth code
The API will now record your unique user ID and mark it as authorized to make further API calls
Go back to step 1, but this time also send the user ID you created. This time you will be able to make the call

## A word on security

All of our NRS servers work in HTTPS mode only, as does our wallet server. This means that all of your calls must be made using HTTPS. Although this is far more secure than plain HTTP, please do not overlook other security measures simply because you are using HTTPS. Remember that you will be sending your account’s password to our server, and that our API uses server-side encryption/decryption – so we recommend setting up a new myNXT.info wallet account if you plan to use the API. Make sure that if you store your myNXT.info wallet username and password in your system and that you do so in the most secure way possible way. Do not keep this information in plain text on your system.

## Making API calls

To make a call to any of the endpoints below, simply append the endpoint to the base URL

`https://wallet.mynxt.info/api/0.1/`

So for example to get all accounts from a user you could do:

`https://wallet.mynxt.info/api/0.1/user/account?email=abc@def.com&password=xyz`

Responses are in JSEND format.

Go to the [Wallet page](wallet.md) to view all endpoints regarding the MyNXT wallet or the to [NXT page](nxt.md) for normal NXT api calls.