# NXT API

Here you can view the NXT API endpoint. [Click here to get started](getting-started.md).

## Usage
`GET nxt?requestType=`
or
`POST nxt?requestType=`

Every call to the original NXT api can be made to the above address replacing everything after the ? with the required parameters as described in the [wiki](http://wiki.nxtcrypto.org/wiki/Nxt_API).
Unless there is an error from our side the return object is exactly the same as described in the [wiki](http://wiki.nxtcrypto.org/wiki/Nxt_API).

### Example

`https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&requestType=getAccount&account=1739068987193023818`

### Response
```
{
    "publicKey": "1259ec21d31a30898d7cd1609f80d9668b4778e3d97e941044b39f0c44d2e51b",
    "requestProcessingTime": 1,
    "guaranteedBalanceNQT": "0",
    "balanceNQT": "-99999730400000000",
    "accountRS": "NXT-MRCC-2YLS-8M54-3CMAJ",
    "account": "1739068987193023818",
    "effectiveBalanceNXT": 0,
    "unconfirmedBalanceNQT": "-99999730400000000",
    "forgedBalanceNQT": "0"
}
```

It's also possible to give a masterPassword and accountId parameter. It will result in the decryption of the wallet and supplying a passPhrase parameter of the corresponding account to the NRS API.

### Example

```
https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&masterPassword=myMasterPassword&accountId=2794585332379340844&requestType=sendMoney&amountNQT=100000000&feeNQT=100000000&deadline=1440&recipient=NXT-38WK-UWYL-MVQY-9PQHV
```

The above example is the same as manually suppling a passPhrase parameter.

```
https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&requestType=sendMoney&passPhrase=mySecretPassPhrase&amount=100000000&feeNQT=100000000&deadline=1440&recipient=NXT-38WK-UWYL-MVQY-9PQHV
```

### Response

```
{
    fullHash: "9f48b14455d6566a0ac7f0ac031eb936e31604adac00583b636d04db0190e8a7",
    requestProcessingTime: 387,
    signatureHash: "e5b8b8dd7f8eb06fcde36f3706cbacd0a0dc624d27251b052c511f20b8d813d5",
    transactionBytes: "00104f96e101a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38919b203dd7d5be7d00e1f5050000000000e1f505000000000000000000000000000000000000000000000000000000000000000000000000e141d58ec260874d72ddbc22b0b6877aeacb02b8109cf467c0ed6702cb3ee702fa16875d72a872fc0cfe54c048e821362c5d10e7a5d3e81f1eb5b66184d6bff70000000008080200c078518b420c0864",
    transaction: "7662547477730511007",
    transactionJSON: {
        fullHash: "9f48b14455d6566a0ac7f0ac031eb936e31604adac00583b636d04db0190e8a7",
        signatureHash: "e5b8b8dd7f8eb06fcde36f3706cbacd0a0dc624d27251b052c511f20b8d813d5",
        transaction: "7662547477730511007",
        amountNQT: "100000000",
        ecBlockHeight: 133128,
        recipientRS: "NXT-38WK-UWYL-MVQY-9PQHV",
        type: 0,
        feeNQT: "100000000",
        recipient: "9060914620736248721",
        version: 1,
        sender: "2794585332379340844",
        timestamp: 31561295,
        ecBlockId: "7208024683551226048",
        height: 2147483647,
        subtype: 0,
        senderPublicKey: "7c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38",
        deadline: 1440,
        senderRS: "NXT-TN3E-KQRH-QE8A-4W4LF",
        signature: "e141d58ec260874d72ddbc22b0b6877aeacb02b8109cf467c0ed6702cb3ee702fa16875d72a872fc0cfe54c048e821362c5d10e7a5d3e81f1eb5b66184d6bff7"
        },
    broadcasted: true,
    unsignedTransactionBytes: "00104f96e101a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38919b203dd7d5be7d00e1f5050000000000e1f505000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008080200c078518b420c0864"
}
```
