# Wallet API

Here you can view all Wallet API endpoints. [Click here to get started](getting-started.md).

## Auth
`POST api/0.1/auth`

### Parameters:

- email required, login email
- password required, login password
- uuid optional, to save device
- gauth_user_code, required if 2fa enabled

Main purpose of this endpoint is to authorize a device to make requests if 2FA is enabled.

After authorizing an uuid it can be used in all API calls below.

### Example

```
var data = {
    email: 'abc@def.com',
    password: 'password',
    uuid: '4cb1fe677ea9e554327b493c85864f8b9bef045202ed36933ef735f46f10fc16'
    gauth_user_code: 123456
}

$.post('https://wallet.mynxt.info/api/0.1/auth', data, function (result) { console.log(result) });
```

### Response

```
{
    "status": "success",
    "data": null
}
```

## Send NXT from wallet
`POST/GET api/0.1/send`

### Parameters

- `email` required, login email
- `password` required, login email
- `uuid optional`, uuid of a authorized device
- `masterPassword` required, wallet password
- `accountId` required, account to send from (from the wallet)
- `recipient` required, account to send to
- `amountNQT` required, amount to send in NQT
- `message` optional, non-encrypted message to attach to transaction (url encoded)
- `messageToEncrypt` optional, encrypted message to attach to transaction (url encoded)

### Example

```
var data = {
    email: 'abc@def.com',
    password: 'password',
    masterPassword: 'masterpassword',
    accountId: '15343849087715339967',
    recipient: '2794585332379340844',
    amountNQT: 100000000
}

$.post('https://wallet.mynxt.info/api/0.1/send', data, function (result) { console.log(result) });
```

### Response

Please note: it always return status: "success" if the NXT api was successfully called. Even if the NRS server returns an error like unsufficient funds.

```
status: "succes",
data: {
        fullHash: "3da5f8ce241593cd6a3dcf789e5005d996dc1e6883d0aa8b540b420482f8048d",
        signatureHash: "f77c6aa23e1fbc0ac8ea1fb330066a6c2110d0676a254ece792e8797a3afdff4",
        transactionBytes: "0000ac723301a0056bfb27389deb2b8c8cc7...",
        transaction: "14813206847187100989",
        broadcasted: true
}
```

It's also possible to use any parameters from the sendMoney api. Additionally you can also attach messages as described here.

### Example

```
var data = {
    email: 'abc@def.com',
    password: 'password',
    masterPassword: 'masterpassword',
    accountId: '15343849087715339967',
    recipient: 'NXT-66LQ-VFM3-8XRW-FBAQE',
    amountNQT: 100000000,
    message: 'mynxt testing'
}

$.post('https://wallet.mynxt.info/api/0.1/send', data, function (result) { console.log(result) });
```

### Response

```
{
    status: "success",
    data: {
        fullHash: "3ba969566ece1d1451e04a005b112a22c9bc0c08f1ee22cd2a3536a307c8169e",
        requestProcessingTime: 366,
        signatureHash: "4870b299de4a49f5d7fbfbdaa9a34bf571a8ce70248fbd39d0102fe28ff0dd49",
        transactionBytes: "0010565fd401a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e3856121266db0959d600e1f5050000000000e1f50500000000000000000000000000000000000000000000000000000000000000000000000045e5ee1b59942e4a634f82d2de11196d11a69b3fbfdee645a46088533555520c446cef585c35aec9e4be8af10fe9ae86a544253ec087b1de5415c269d00ade7c01000000ffe001006628f624bf5d7956010d0000806d796e78742074657374696e67",
        transaction: "1449541628374657339",
        transactionJSON: {
            fullHash: "3ba969566ece1d1451e04a005b112a22c9bc0c08f1ee22cd2a3536a307c8169e",
            signatureHash: "4870b299de4a49f5d7fbfbdaa9a34bf571a8ce70248fbd39d0102fe28ff0dd49",
            transaction: "1449541628374657339",
            amountNQT: "100000000",
            ecBlockHeight: 123135,
            attachment: {
                message: "mynxt",
                version.Message: 1,
                messageIsText: true
            },
            recipientRS: "NXT-66LQ-VFM3-8XRW-FBAQE",
            type: 0,
            feeNQT: "100000000",
            recipient: "15445387234958774870",
            version: 1,
            sender: "2794585332379340844",
            timestamp: 30695254,
            ecBlockId: "6231114634984040550",
            height: 2147483647,
            subtype: 0,
            senderPublicKey: "7c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38",
            deadline: 1440,
            senderRS: "NXT-TN3E-KQRH-QE8A-4W4LF",
            signature: "45e5ee1b59942e4a634f82d2de11196d11a69b3fbfdee645a46088533555520c446cef585c35aec9e4be8af10fe9ae86a544253ec087b1de5415c269d00ade7c"
        },
        broadcasted: true,
        unsignedTransactionBytes: "0010565fd401a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e3856121266db0959d600e1f5050000000000e1f5050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000ffe001006628f624bf5d7956010d0000806d796e78742074657374696e67"
    }
}
```

## Get encrypted wallet
`GET api/0.1/user/wallet`

### Parameters:

- `email` required, login email
- `password` required, login email
- `uuid` optional, uuid of a authorized device

### Example

`https://wallet.mynxt.info/api/0.1/user/wallet?email=abc@def&password=password`

### Response

```
{
    "status": "success",
    "data": {
        "wallet": "...",
        "salt": "...",
        "version": "2",
        "accounts": [{
            "id_account": "132",
            "tx_account_id": "11242899405565095088",
            "tx_label": "Main account",
            "bl_active": "1",
            "bl_selected": "1"
        }, {
            "id_account": "131",
            "tx_account_id": "12503010641868735489",
            "tx_label": "Secondary account",
            "bl_active": "1",
            "bl_selected": "0"
        }]
    }
}
```

## Get all accounts from user
`GET api/0.1/user/account`

### Parameters:

- `email` required, login email
- `password` required, login email
- `uuid` optional, uuid of a authorized device

### Example

`https://wallet.mynxt.info/api/0.1/user/acccount?email=abc@def&password=password`

### Response
```
{
    "status": "success",
    "data": {
        "accounts": [{
            "id_account": "132",
            "tx_account_id": "11242899405565095088",
            "tx_label": "Main account",
            "bl_active": "1",
            "bl_selected": "1"
        }, {
            "id_account": "131",
            "tx_account_id": "12503010641868735489",
            "tx_label": "Secondary account",
            "bl_active": "1",
            "bl_selected": "0"
        }]
    }
}
```

## Get tickers
`GET api/0.1/rates`

### Parameters:

- `email` required, login email
- `password` required, login email
- `uuid` optional, uuid of a authorized device

### Example

https://wallet.mynxt.info/api/0.1/rates?email=abc@def&password=password

### Response

```
{
    "status": "success",
    "data": [{
        "ticker": "BTC_USD",
        "value": "610.94",
        "updated": "2014-07-16 19:40:10",
    }, {
        "ticker": "BTC_NXT",
        "value": "0.0000714",
        "updated": "2014-07-16 19:40:10",
    }]
}
```

## NXT API
`GET nxt?requestType=`

Every call to the original NXT api can be made to the above address replacing everything after the ? with the required parameters as described in the wiki. Unless there is an error from our side the return object is exactly the same as described in the wiki

### Example

`https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&requestType=getAccount&account=1739068987193023818`

### Response
```
{
    "publicKey": "1259ec21d31a30898d7cd1609f80d9668b4778e3d97e941044b39f0c44d2e51b",
    "requestProcessingTime": 1,
    "guaranteedBalanceNQT": "0",
    "balanceNQT": "-99999730400000000",
    "accountRS": "NXT-MRCC-2YLS-8M54-3CMAJ",
    "account": "1739068987193023818",
    "effectiveBalanceNXT": 0,
    "unconfirmedBalanceNQT": "-99999730400000000",
    "forgedBalanceNQT": "0"
}
```

It's also possible to give a masterPassword and accountId parameter. It will result in the decryption of the wallet and supplying a passPhrase parameter of the corresponding account to the NRS API.

### Example

```
https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&masterPassword=myMasterPassword&accountId=2794585332379340844&requestType=sendMoney&amountNQT=100000000&feeNQT=100000000&deadline=1440&recipient=NXT-38WK-UWYL-MVQY-9PQHV
```

The above example is the same as manually suppling a passPhrase parameter.

```
https://wallet.mynxt.info/api/0.1/nxt?email=abc@def&password=password&requestType=sendMoney&passPhrase=mySecretPassPhrase&amount=100000000&feeNQT=100000000&deadline=1440&recipient=NXT-38WK-UWYL-MVQY-9PQHV
```

### Response

```
{
    fullHash: "9f48b14455d6566a0ac7f0ac031eb936e31604adac00583b636d04db0190e8a7",
    requestProcessingTime: 387,
    signatureHash: "e5b8b8dd7f8eb06fcde36f3706cbacd0a0dc624d27251b052c511f20b8d813d5",
    transactionBytes: "00104f96e101a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38919b203dd7d5be7d00e1f5050000000000e1f505000000000000000000000000000000000000000000000000000000000000000000000000e141d58ec260874d72ddbc22b0b6877aeacb02b8109cf467c0ed6702cb3ee702fa16875d72a872fc0cfe54c048e821362c5d10e7a5d3e81f1eb5b66184d6bff70000000008080200c078518b420c0864",
    transaction: "7662547477730511007",
    transactionJSON: {
        fullHash: "9f48b14455d6566a0ac7f0ac031eb936e31604adac00583b636d04db0190e8a7",
        signatureHash: "e5b8b8dd7f8eb06fcde36f3706cbacd0a0dc624d27251b052c511f20b8d813d5",
        transaction: "7662547477730511007",
        amountNQT: "100000000",
        ecBlockHeight: 133128,
        recipientRS: "NXT-38WK-UWYL-MVQY-9PQHV",
        type: 0,
        feeNQT: "100000000",
        recipient: "9060914620736248721",
        version: 1,
        sender: "2794585332379340844",
        timestamp: 31561295,
        ecBlockId: "7208024683551226048",
        height: 2147483647,
        subtype: 0,
        senderPublicKey: "7c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38",
        deadline: 1440,
        senderRS: "NXT-TN3E-KQRH-QE8A-4W4LF",
        signature: "e141d58ec260874d72ddbc22b0b6877aeacb02b8109cf467c0ed6702cb3ee702fa16875d72a872fc0cfe54c048e821362c5d10e7a5d3e81f1eb5b66184d6bff7"
        },
    broadcasted: true,
    unsignedTransactionBytes: "00104f96e101a0057c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38919b203dd7d5be7d00e1f5050000000000e1f505000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008080200c078518b420c0864"
}
```
