# Plugins

MyNXT has a powerful plugin system that will allow everyone to build plugins for NXT.

---

## Example plugin

You can find an example of a plugin at [BitBucket](https://bitbucket.org/mynxt-team/example-plugin/).

## Libraries

We provide two libraries. A [jQuery library](jquery.md) for everyone and a [AngularJS service](angular.md) for people that prefer Angular. For large plugins we recommend Angular.