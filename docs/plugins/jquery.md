# jQuery library

---

You can find the library in our [BitBucket repository](https://bitbucket.org/mynxt-team/jquery-library/)

## Get accounts

You can retrieve all accounts via `MyNXT.getAccounts(callback)`. The callback takes one parameter: `result`.

### Usage
```
MyNXT.getAccounts(function (result) {});
```

### Response
```
{
   "status":"success",
   "data":{
      "accounts":[
         {
            "id_account":"101",
            "tx_account_id":"2794585332379340844",
            "tx_public_key":"7c7de7102737004cc64f9f75736cbbfd76f5f782f25be0a11c40658fc6a09e38",
            "tx_label":"Main account",
            "bl_active":"1",
            "bl_selected":"1"
         },
         {
            "id_account":"102",
            "tx_account_id":"15445387234958774870",
            "tx_public_key":"38ef44852dffdbf20c501464d51ecf86d9f4d15798ee62f53352e25fbe19161e",
            "tx_label":"Savings account",
            "bl_active":"1",
            "bl_selected":"0"
         }
      ]
   }
}
```

- `id_account` internal account ID
- `tx_account_id` NXT account ID of this account
- `tx_public_key` Public key of this account
- `tx_label` Label of this account
- `bl_active` Is this account active. Will be the case 99.99% of the time
- `bl_selected` Is this the current selected account in the wallet.

### NRS Requests

You can also make any NRS requests as described in the [wiki](wiki.nxtcrypto.org/wiki/Nxt_API) using `MyNXT.nrsRequest(requestType, data, callback)`

#### Usage
```
MyNXT.nrsRequest('getTransaction', { transaction: 123342896693901042 }, function (result) {});
```

#### Response

The responses will be exactly the same as described in the wiki.

```
{
   "signature":"2aeabaabc18d0e38ba38e586103c0a8461afcff566eb24f741fe62220f9bc602c808ab44704764996588fd0745330f08ca88f9433fd02e19b07fd62795176701",
   "transactionIndex":0,
   "type":0,
   "ecBlockId":"5556085300633820589",
   "signatureHash":"595c4c142db88a7d23c707f18ced0c82d364ce0f96292e92dfc4f7b1dc93213c",
   "attachment":{
      "encryptedMessage":{
         "data":"3d8376fabc5198dfab6a96aadd71ca09d1b6ec4127911f2e2d845e6e0c035876a54facc91fa8a084f0abff67f655a8923a8f1f7eb374f3dddfcae2af5f2ac077",
         "nonce":"665f62470968c8d633c8309648efa36ed388a0dcd4de7924006bbeaac383a1e8",
         "isText":true
      },
      "version.EncryptedMessage":1
   },
   "senderRS":"NXT-AMPQ-B6ZZ-S8TL-EFDBH",
   "subtype":0,
   "amountNQT":"509900000000",
   "recipientRS":"NXT-E8JD-FHKJ-CQ9H-5KGMQ",
   "block":"17850083479769392422",
   "blockTimestamp":27626034,
   "deadline":1440,
   "timestamp":27625922,
   "height":86966,
   "senderPublicKey":"dc7b03d7dd03fe316bb3321c65683c7e528dbed6bdedd67d5aeae2e3dd170126",
   "feeNQT":"100000000",
   "requestProcessingTime":1,
   "confirmations":69465,
   "fullHash":"f2f60107b633b60175d64c87e6d580cd2cbdf3521a7f02cbfd9e2af88f4714e5",
   "version":1,
   "sender":"14386024746077933238",
   "recipient":"4273301882745002507",
   "ecBlockHeight":179517,
   "transaction":"123342896693901042"
}
```

## Create transaction

You can ask to create any transaction on behalf of the user. This can be any NRS call like `sendMoney`, `sendMessage`, `placeBidOrder` etc.


### Usage
```
 var data = {
   requestType: 'sendMoney',
   amountNQT: 100000000, // 1 NXT
   feeNQT: 100000000, // optional, we will use 1 NXT if left omitted
   recipient: 'NXT-E8JD-FHKJ-CQ9H-5KGMQ' // required for this requestType
 };

MyNXT.sendTransaction(data, function (result) {});
```
### Request

Besides the actual NRS parameters there are specific parameters to MyNXT. This list could change in the future.

- `account` this is the account to use in numeric format, this must be an account in the user's MyNXT wallet. See the [Get accounts](#get-accounts) endpoint to retrieve accounts.

### Response

The responses will be exactly the same as described in the wiki. If the user cancelled the response will be `{"success": false }`.

## Create website token

You can also create an website token on behalf of the user.


### Usage
```
 var data = {
   requestType: 'generateToken',
   website: 'mynxt.info'
 };

MyNXT.sendTransaction(data, function (result) {});
```
### Request

- `requestType` must be `generateToken`
- `website` website or message to generate a token for
- `account` this is the account to use in numeric format, this must be an account in the user's MyNXT wallet. See the [Get accounts](#get-accounts) endpoint to retrieve accounts.

### Response

The responses will be exactly the same as [described in the wiki](http://wiki.nxtcrypto.org/wiki/Nxt_API#Generate_Token). If the user cancelled the response will be `{"success": false }`.

```
{
   "requestProcessingTime":0,
   "token":"4s8eevbs9v34o01nndm76tcvgbrvatnt3igu0mvik338upa01ud98e4u2gnl3jo2mkgrmo3dqus8k9464542cqfuejb0f0odgb8j9gib18ignam5qq5jjbugsh6kop01saf8qi7junknm9b1bopdsth1jm4h07ai"
}
```

## Local development

When developing the plugin in a local environment it's not possible to create the transaction modal since it's on the MyNXT servers.

API calls need to made with your account. To solve this problems it's possible to create a config object before including the library. All API calls will be made with your information.

This config should never be public

```
var MyNXT = {};

// Configuration object for local development
MyNXT.sdkConfig = {
   email: 'email@email.com',
   password: 'password'
};
```

## iFrame resizer

To make sure your plugin is viewed correctly in our store we use a library called [iFrame Resizer](https://github.com/davidjbradshaw/iframe-resizer).

This library is included in our [example plugin](https://bitbucket.org/mynxt-team/example-plugin).