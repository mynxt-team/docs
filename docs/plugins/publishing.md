# Publishing plugins

---

Plugins are client-side (HTML + JS) applications that can interact with the MyNXT Wallet in a secure way. The plugin runs inside an IFRAME to isolate it from the rest of the wallet, but it can initiate NXT transactions with the user's MyNXT accounts by calling a special API endpoint that prompts the user for their master password, makes the NXT API call in behalf of the plugin and then return the results to the plugin.

This process offers the best security, since the plugin itself never gets access to the user's NXT account passphrase or their MyNXT master password. All plugins will be reviewed by MyNXT before being published.

### Rules

- It is permitted for your plugin to make AJAX calls to an external website/services
- All plugins need to be open source (they would be anyway since they use HTML+JS)
- It is not allowed to ask the user for their MyNXT account pass phrase or master password under any circumstance
- If your plugin is paid, MyNXT follows the same model as app stores, we charge a 30% fee and the plugin author keeps 70%
- We reserve the right to un-publish your plugin in case we find that it is breaking any of the MyNXT rules (this is to ensure the security of MyNXT and its users).

If you are in doubt about whether your idea would be a good fit for MyNXT, please contact us first and we will be happy to discuss it with you.

## Publishing process

1. Read all the docs first, ask any question you still have by contact us or in the [forums](https://nxtforum.org/mynxt-info/).
2. Create a repository for your plugin in [GitHub](https://github.com/) or [BitBucket](https://bitbucket.org) (it's free) and submit the URL in the developers section of the MyNXT plugins area.
3. Make sure your plugin has the [required information listed below](#required-information).
4. Make sure you include a whitelist.txt file listing all external websites that are allowed to receive AJAX calls from your plugin (one domain per line, wildcards allowed).
5. MyNXT will review the plugin (lead time is 1 week), and make a private copy of your repository which is the one we will offer to users.
6. If you need to update your plugin, you should update the files in the register repository and submit a new version of the plugin. We will review it again before publishing.

## Required information

Before we can publish the plugin we will need the following information about the plugin:

- `author_name` under which name should the plugin be published
- `author_website` *optional* website of the author
- `author_email` *optional* email of the author
- `title` the name of the plugin as it will appear in the store
- `menu_name` the name of the plugin as it will appear in wallet menu
- `short_description` short, 60 characters max, description
- `description` main description as it will appear in the details modal, can include HTML
- `logo_path` path of the logo in the repository, for example `app/img/logo.png`
- `price_nxt` price in NXT of the plugin, this can either be 0 or a minimal of 5 NXT.
- `version` current version of the plugin

You can submit this information in a txt file, json file, xml file. As long as it nicely formatted and clear.