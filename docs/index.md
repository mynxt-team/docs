# MyNXT.info docs

Welcome to the developers area of MyNXT. Our goal is to make it easy for any developer to create and maintain new applications for NXT. We take care of making sure that our servers keep running, are updated and secure so that you can concentrate just on creating amazing apps!

All the tools in the developers area of MyNXT are free to use. We only ask you to be responsible and to not create anything that is illegal (no porn, drugs or gambling please). Also, while we don't apply any limits to the amount of calls you can make to the API, please try to keep it to about 30 calls per minute at the most. If we find you hammering our API with too many calls, your API access may be blocked. If you think you need a higher limit, please contact us and we will do our best to accommodate your needs.

The following products are available for developers:

## API's

- [Getting started](api/getting-started.md)
- [MyNXT API](api/wallet.md): Interact with your MyNXT account
- [NXT API](api/nxt.md): Interact with the standard NXT API through your MyNXT account

## Plugins

Plugins allows you to create apps (plugins) that can be offered for free or paid in the MyNXT Online Wallet.

- [Getting started with plugins](plugins/getting-started.md)
- [Example plugin source](https://bitbucket.org/mynxt-team/example-plugin)
- [Asset plugin source](https://bitbucket.org/mynxt-team/asset-plugin-pro)